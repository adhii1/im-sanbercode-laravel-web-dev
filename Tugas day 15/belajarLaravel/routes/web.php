<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']); 
Route::get('/register', [AuthController::class, 'register']); 
Route::post('/welcome', [AuthController::class, 'halaman']); 

Route::get('/data-table', function(){
    return view('pages.data-table');
});

Route::get('/table', function(){
    return view('pages.table');
});

// create data
// menuju ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//masukan data ke DB
Route::post('/cast', [CastController::class, 'store']);

// read data
Route::get('/cast', [CastController::class, 'index']);
// read data berdasarkan parameter id
Route::get('/cast/{id}', [CastController::class, 'show']);

// update data
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// update data di DB berdasarakan id
Route::put('/cast/{id}', [CastController::class, 'update']);

// delete data berdasarkan paramater ID
Route::delete('/cast/{$id}', [CastController::class, 'destroy']);