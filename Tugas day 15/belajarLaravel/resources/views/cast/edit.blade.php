@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')

<form action="/cast/{{$casty->id}}" method="POST">
    @csrf
    @method('put')
    {{-- Validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="form-group">
      <label for="exampleInputEmail1">Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$casty->nama}}" aria-describedby="emailHelp">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" class="form-control" name="umur" value="{{$casty->umur}}">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <textarea name="bio" class="form-control" name="bio">{{$casty->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary btn-warning">Update</button>
  </form>

@endsection