@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')

    <a class="btn btn-primary btn-sm my-3" href="/cast/create">Tambah</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">nama</th>
            <th scope="col">action</th>
          </tr>
        </thead>
            @forelse ($cast as $index => $casty)
            <tbody>
                <tr>
                <th scope="row">{{$index + 1}}</th>
                <td>{{$casty->nama}}</td>
                <td>
                    <form action="/cast/{{$casty->id}}" method="POST">
                    <a href="/cast/{{$casty->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$casty->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" value=Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>   
            </tr>
            @empty
              <tr>  
                <td>Tidak ada Cast</td>
            </tr>
        </tbody>
            @endforelse
           
      </table>

@endsection