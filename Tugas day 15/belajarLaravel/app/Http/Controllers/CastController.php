<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request)
    // validation
    {
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // tambah
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast');
    }
        public function index()
        {
            $cast = DB::table('cast')->get();
            
            return view('cast.tampil', ['cast' => $cast]);
        }

        public function show($id){

            $casty = DB::table('cast')->find($id);
            
            return view('cast.detail', ['casty' => $casty]);
        }

        public function edit($id){

            $casty = DB::table('cast')->find($id);
            
            return view('cast.edit', ['casty' => $casty]);
        }

        public function update($id, Request $request){
            // validasi
            $request->validate([
                'nama' => 'required|min:5',
                'umur' => 'required',
                'bio' => 'required',
            ]);

        // update
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
                
            );

            return redirect('/cast');
        }

        public function destroy($id)
        {
            DB::table('cast')->where('id',$id)->delete();

            return redirect('/cast');
        }
    }
    
