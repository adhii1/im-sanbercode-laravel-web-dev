<?php
require_once('animal.php');

class Frog extends Animal{
    public function lompat($jump)
    {
        echo "Jump : $jump <br> <br>"; 

    }
}

class Ape extends Animal{
    public $legs = 2;

    public function kera($yell){
        echo "Yell : $yell <br> <br>";
    }
}

?>