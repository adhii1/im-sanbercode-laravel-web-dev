<?php
require_once('animal.php');
require_once('animals.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->jenis . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$kodok = new Frog("buduk");

echo "Name : " . $kodok->jenis . "<br>"; // "buduk"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo $kodok->lompat("hop-hop");

$sungokong = new Ape("Kera Sakti");

echo "Name : " . $sungokong->jenis . "<br>"; // "Kera Sakti"
echo "legs : " . $sungokong->legs . "<br>"; // 2
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo $sungokong->kera("Auooo");

?>