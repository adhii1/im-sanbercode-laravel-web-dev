<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biodata</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <label>First Name:</label> <br> <br>
    <input type="text" name="fname"> <br> <br>
    <label>Last Name:</label> <br> <br>
    <input type="text" name="lname"> <br> <br>
    <label>Gender:</label> <br> <br>
    <input type="radio">Male <br>
    <input type="radio">Female <br>
    <input type="radio">Other <br> <br>
    <Label>Nationality</Label> <br> <br>
    <select name="Nationality"> <br> <br>
        <option value="Indonesian">Indonesian</option> <br>
        <option value="English">English</option> <br>
        <option value="Other">Other</option> <br>
    </select> <br> <br>
    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br> <br>
    <label>Bio:</label> <br> <br>
    <textarea name="Bio" cols="30" rows="10"></textarea> <br> <br>
    <input type="submit">
    </form>
</body>
</html>