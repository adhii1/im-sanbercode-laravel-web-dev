<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view ('pages.register');
    }

    public function halaman(Request $request)
    {
        $namaDepan = $request->input("fname");
        $namaBelakang = $request->input("lname");

        return view ('pages.welcome',["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
