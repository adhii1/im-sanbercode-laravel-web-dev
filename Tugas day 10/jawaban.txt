1. Buatlah database dengan nama "myshop"
CREATE DATABASE myshop;

2. Buat Table
-users
CREATE TABLE users(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(255),
email varchar(255),
password varchar(255)
);

-categories
CREATE TABLE categories(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(255),
)

-items
CREATE TABLE items(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    description varchar(255) NOT null,
    price integer NOT null,
    stock integer NOT null,
    categories_id int(11)NOT null,
    FOREIGN KEY(categories_id) REFERENCES categories(id)
)

3. INSERT DATA
-users
INSERT INTO users(name, email, password) VALUES("john Doe", "john@doe.com", "john123")
INSERT INTO users(name, email, password) VALUES("jane Doe", "jane@doe.com", "jane123")

-categories
INSERT INTO categories(name) VALUES("Gadget"), ("Cloth"), ("Men"), ("Women"), ("Branded");

-items
INSERT INTO items(name, description, price, stock, categories_id) VALUES ("sumsang b50", "hape keren dari merk sumsang", 4000000, 100,1), ("uniklooh", "baju keren dari brand ternama", 500000,50,2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10,1);

4. 
-4a
SELECT id, name, email FROM users;
-4b
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE "%watch%";


